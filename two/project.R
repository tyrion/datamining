coronary <- read.table("http://www.cs.uu.nl/docs/vakken/mdm/coronary.txt", T)
rhc.data <- read.csv("http://www.cs.uu.nl/docs/vakken/mdm/rhc-small.txt", T)
ct  <- table(coronary) #table of counts for the coronary dataset
rhc <- table(rhc.data) #table of counts for the rhc dataset

gm.search <- function(data, graph, forward=T, backward=T, score.name,
                      scorefn = get(score.name)) {
  ## Performs a local search hill-climbing starting from some given model (graph)
  ##
  ## Args:
  ##   data: Table, with the observed counts
  ##   graph: Matrix, the model (Undirected Independence Graph) to start from, in the form of a diagonally-symmetrical matrix
  ##   forward: Boolean, whether to include the addition of a single edge in the heighborhood-space
  ##   backward: Boolean, whether to include the deletion of a single edge in the heighborhood-space
  ##   scorefn: Function, the scoring function to use
  ##
  ## Returns:
  ##   A named list with
  ##     $cliques the cliques that form the model corresponding to the local optimum reached
  ##     $score the score of the model corresponding to the local optimum reached
  ##     $trace dataframe, with information about the steps that are made in each iteration of the local search
  ##     $call The call to the function gm.search that produced this result
  ##     $graph Matrix, corresponding to the model reached as local optimum
  dim <- dim(graph)[1]
  smc <- length(data)
  N   <- sum(data)
  
  trace <- data.frame(op=character(), from=integer(), to=integer(),
                      score=double(), stringsAsFactors=FALSE)
  write.trace <- function(op, i, score)
    ## Writes the information about the step made in this iteration to the trace
    trace[nrow(trace)+1,] <<- list(DIRS[op+1], i %/% dim +1, i %% dim +1, score)
  
  score <- function(data, graph, i=NA, y=NA) {
    ## Computes the score that corresponds to fitting the given model to the given observed counts by Iterative Proportianal Fitting.
    ##
    ## Args:
    ##   data: Table, with the observed counts
    ##   graph: Matrix, the model to score
    ##   i: Integer, index of last change
    ##   y: Integer, last change (add [1] or delete [0])
    ##
    ## Returns:
    ##   List containing the model (as cliques), information about the last change and the computed score
    cliques <- post.process(find.cliques(graph))
    results <- loglin(data, cliques, print=F)
    score <- scorefn(graph, results$lrt, smc - results$df, N)
    list(cliques=cliques, i=i, y=y, score=score)
  }
  
  search <- function(data, graph, x, y) {
    for (i in which(graph == x & lower.tri(graph))) {
      set(graph, i, dim) <- y
      curr  <- score(data, graph, i, y)
      if (curr$score < best$score) best <<- curr
      set(graph, i, dim) <- x
    }
  }
  
  init <- score(data, graph)
  best <- init
  
  repeat {
    if (forward ) search(data, graph, DEL, ADD)
    if (backward) search(data, graph, ADD, DEL)
    
    if (best$score == init$score)
      break
    
    set(graph, best$i, dim) <- best$y
    init <- best
    write.trace(best$y, best$i - 1, best$score)
  }
  
  list(cliques=best$cliques, score=best$score, trace=trace, call=match.call(),
       graph=graph)
}


gm.restart <- function(nstart, prob=0.5, seed=.Random.seed, data, forward=T, 
                       backward=T, score="bic") {
  ## Performs a local search with restarts
  ##
  ## Args:
  ##   nstart: Integer, number of restarts
  ##   prob: Float, probability of an edge being present in a starting model
  ##   seed: Integer, set the seed
  ##   data: Table, observed counts
  ##   forward: Boolean, whether to include the addition of a single edge in 
  ##            the heighborhood-space
  ##   backward: Boolean, whether to include the deletion of a single edge in 
  ##            the heighborhood-space
  ##   score: Function, the scoring function to use
  ##
  ## Returns:
  ##   A named list with
  ##     $cliques the cliques that form the model corresponding to the best 
  ##        local optimum reached
  ##     $score the score of the model corresponding to the best local 
  ##        optimum reached
  ##     $trace dataframe, with information about the steps that are made in 
  ##        each iteration of the local search that resulted in the best optimum
  ##     $call The call to the function gm.search that produced this result
  ##     $graph Matrix, corresponding to the model reached as best local optimum 
  
  set.seed(seed)
  dim <- length(dim(data))
  best <- gm.search(data, random.graph(dim, prob), forward, backward, score)
  for (i in integer(nstart - 1)) {
    curr <- gm.search(data, random.graph(dim, prob), forward, backward, score)
    best <- if (curr$score < best$score) curr else best
  }
  best
}

random.graph <- function(dim, prob=0.5) {
  ## Constructs a random Undirected Independence Graph
  ##
  ## Args:
  ##   dim: Integer, number of nodes
  ##   prob: Float, probability of an edge being present
  ##
  ## Returns:
  ##    Matrix that represents the Undirected Independence graph. 
  ##    The Matrix is diagonally Symmetrical
  m <- matrix(0, dim, dim)
  for (i in which(lower.tri(m)))
    set(m, i, dim) <- sample(0:1, 1, prob=c(1 - prob, prob))
  m
}

"set<-" <- function(graph, i, len = dim(graph)[1], value) {
  ## Sets the edge presence given a 1-dimensional matrix index preserving
  ##    Matrix symmetricity
  ##
  ## Args:
  ##   graph: Matrix, representing the graph
  ##   i: 1-dimensinal indexer
  ##   value: Integer, value to set it to (present [1] or absent [0])
  ##
  ## Returns
  ##   the new graph
  i <- i - 1
  x <- i %/% len + 1
  y <- i %%  len + 1
  graph[x, y] <- value
  graph[y, x] <- value
  graph
}


ADD = 1
DEL = 0
DIRS = c("DEL", "ADD")

## AIC and BIC scoring functions
aic <- function(graph, dev, dim, N) dev + 2 * dim
bic <- function(graph, dev, dim, N) dev + log(N) * dim

#code from teacher
find.cliques <- function (graph, R = c(), P = 1:dim(graph)[1], X = c(), 
                          cliques = list())
{
  if (length(P)==0 & length(X)==0) {cliques <- list(R)}
  else {
    pivot <- P[sample(length(P),1)]
    for(i in 1:length(P)){
      pivot.nb <- neighbors(graph,pivot)
      if(!is.element(P[i],pivot.nb)){
        P.temp <- setdiff(P,P[i])
        R.new <- union(R,P[i])
        P.new <- intersect(P.temp,neighbors(graph,P[i]))
        X.new <- intersect(X,neighbors(graph,P[i]))
        cliques <- c(cliques, find.cliques(graph, R.new, P.new, X.new))
        X <- union(X,P[i])}
    }}
  cliques
}

#code from teacher
neighbors <- function(graph,node)
{
  nnodes <- dim(graph)[2]
  (1:nnodes)[graph[node,]==1]
}

#code from teacher
post.process <- function(cliques)
{
  unique(lapply(cliques,sort))
}

runall  <- function (data) {
  library(igraph)
  mapply(function(prob, name) {
    score <- get(name)
    x <- gm.search(table(data), random.graph(ncol(data), prob), score=score)
    
    fname <- sprintf("gm-search-%d-%s", prob, name)
    dump("x", sprintf("%s.txt", fname))
    
    colnames(x$graph) <- names(data)
    g <- graph_from_adjacency_matrix(x$graph, "undirected", diag=F)
    
    svg(filename = sprintf("%s.svg", fname))
    plot(g, vertex.size = 40, vertex.color="forestgreen",
         vertex.label.color="Khaki", vertex.label.family="ubuntu",
         vertex.label.font=2, vertex.frame.color="darkgreen", edge.width=4,
         edge.arrow.mode=3)
    dev.off()
  }, c(0, 0, 1, 1), c("aic", "bic"))
}